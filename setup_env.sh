sudo apt install bash-completion tmux pspg
sudo apt install python3-pip
pip3 install pgcli
PATH=$PATH:~/.local/bin/pgcli
sudo apt install curl neovim

ln --force --symbolic ~/dotfiles/.gitconfig ~/.gitconfig
ln --force --symbolic ~/dotfiles/.psqlrc ~/.psqlrc
ln --force --symbolic ~/dotfiles/.tmux.conf ~/.tmux.conf
curl -fLo ~/.local/share/nvim/site/autoload/plug.vim --create-dirs https://raw.githubusercontent.com/junegunn/vim-plug/master/plug.vim
mkdir --parents ~/.config/nvim/ && ln --force --symbolic ~/dotfiles/nvim/init.vim ~/.config/nvim/init.vim
mkdir --parents ~/.config/pgcli/ && ln --force --symbolic ~/dotfiles/pgcli/config ~/.config/pgcli/config
mkdir --parents ~/.ssh && ln --force --symbolic ~/dotfiles/.ssh/config ~/.ssh/config

git() { if [[ $@ == "graph" ]]; then command git log --graph --pretty=oneline --abbrev-commit; else command git "$@"; fi; }
