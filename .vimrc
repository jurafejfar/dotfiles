"byobu setting
nnoremap [5;7~ :tabprevious<CR>
nnoremap [6;7~ :tabnext<CR>
"
""toggle insert mode
set pastetoggle=<Insert>

set ruler
set number

map <C-t> :NERDTreeToggle<CR>

set clipboard=unnamedplus

syntax on

vmap <F5> <Plug>SendSelectionToTmux
nmap <F5> <Plug>NormalModeSendToTmux
